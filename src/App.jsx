import NavBar from "./Sections/NavBar";
import Heading from "./Sections/Heading";
import Stat from "./Sections/Stat";
import Calendar from "./Sections/Calendar";
import ToDoList from "./Sections/ToDoList";
import ProjectList from "./Sections/ProjectList";
import Graph from "./Sections/Graph";
import PiChart from "./Sections/PiChart"
function App() {
  return (
    <div className="App">
      <NavBar></NavBar>
      <div className="container-fluid">
        <Heading></Heading>
        <Stat></Stat>
        <div className="row">
          <Calendar></Calendar>
          <div className="col-lg-6 col-md-12 col-sm-12 mb-4">
            <br /><br />
            <ToDoList></ToDoList>
            <br />
            <ProjectList></ProjectList>
          </div>
        </div>
        <div className="row">
          <Graph></Graph>
          <PiChart></PiChart>
        </div>

      </div>
    </div>
  );
}

export default App;
