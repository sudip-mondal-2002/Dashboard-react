const ProjectItem = (props) => {
    return (
        <div>
            <h4 className="small font-weight-bold">
                {props.heading}
                <span className="float-right">{props.progress+"%"}</span></h4>
            <div className="progress mb-4">
                <div className="progress-bar bg-danger" aria-valuenow={props.progress} aria-valuemin="0"
                    aria-valuemax="100" style={{width : props.progress+"%"}}><span className="sr-only">{props.progress+"%"}</span>
                </div>
            </div>
        </div>
    )
}
export default ProjectItem;