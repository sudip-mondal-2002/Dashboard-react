const ToDoItem = (props) => {
    return (
        <li className="list-group-item">
            <div className="row align-items-center no-gutters">
                <div className="col mr-2">
                    <h6 className="mb-0">
                        <strong>{props.heading}</strong>
                    </h6>
                    <span className="text-xs">{props.time}</span>
                </div>
                <div className="col-auto">
                    <div className="custom-control custom-checkbox">
                        <input className="custom-control-input" type="checkbox" id={"form-check="+props.index}/>
                        <label className="custom-control-label" htmlFor={"form-check="+props.index} ></label>
                    </div>
                </div>
            </div>
        </li>
    )
}

export default ToDoItem;