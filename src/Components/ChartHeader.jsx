const ChartHeader = (props) => {
    return (
        <div className="card-header d-flex justify-content-between align-items-center">
            <h6 className="text-primary font-weight-bold m-0">{props.heading}</h6>
            <div className="dropdown no-arrow">
                <button className="btn btn-link btn-sm dropdown-toggle" aria-expanded="false" data-toggle="dropdown" type="button">
                    <i className="fas fa-ellipsis-v text-gray-400"></i>
                </button>
                <div className="dropdown-menu shadow dropdown-menu-right animated--fade-in">
                    <p className="text-center dropdown-header">{props.DropHeader}</p>
                    <a className="dropdown-item" href="/">&nbsp;{props.Action1}</a>
                    <a className="dropdown-item" href="/">&nbsp;{props.Action2}</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="/">&nbsp;{props.Other}</a>
                </div>
            </div>
        </div>
    )
}
export default ChartHeader;