const PiChartBody = () => {
    return (
        <div className="card-body">
            <div className="chart-area"><canvas
                data-bss-chart="{&quot;type&quot;:&quot;doughnut&quot;,&quot;data&quot;:{&quot;labels&quot;:[&quot;Direct&quot;,&quot;Social&quot;,&quot;Referral&quot;],&quot;datasets&quot;:[{&quot;label&quot;:&quot;&quot;,&quot;backgroundColor&quot;:[&quot;#4e73df&quot;,&quot;#1cc88a&quot;,&quot;#36b9cc&quot;],&quot;borderColor&quot;:[&quot;#ffffff&quot;,&quot;#ffffff&quot;,&quot;#ffffff&quot;],&quot;data&quot;:[&quot;50&quot;,&quot;30&quot;,&quot;15&quot;]}]},&quot;options&quot;:{&quot;maintainAspectRatio&quot;:false,&quot;legend&quot;:{&quot;display&quot;:false},&quot;title&quot;:{}}}"></canvas>
            </div>
            <div className="text-center small mt-4">
                <span className="mr-2"><i className="fas fa-circle text-primary"></i>&nbsp;Direct</span>
                <span className="mr-2"><i className="fas fa-circle text-success"></i>&nbsp;Social</span>
                <span className="mr-2"><i className="fas fa-circle text-info"></i>&nbsp;Refferal</span>
            </div>
        </div>
    )
}

export default PiChartBody;