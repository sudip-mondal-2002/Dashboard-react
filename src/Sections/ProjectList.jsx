import ProjectItem from "../Components/ProjectItem";

const ProjectList = () => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="text-primary font-weight-bold m-0">Projects</h6>
            </div>
            <div className="card-body">
                <ProjectItem heading="Enable Authentication" progress="20"></ProjectItem>
                <ProjectItem heading="Account Setup" progress="60"></ProjectItem>
                <ProjectItem heading="Server Migration" progress="80"></ProjectItem>
                <ProjectItem heading="Sales Tracking" progress="20"></ProjectItem>
                <ProjectItem heading="Customer Database" progress="10"></ProjectItem>
                <ProjectItem heading="PayOut Details" progress="90"></ProjectItem>
            </div>
        </div>
    )

}
export default ProjectList