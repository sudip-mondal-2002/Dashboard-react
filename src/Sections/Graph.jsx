import ChartHeader from "../Components/ChartHeader";
import GraphBody from "../Components/GraphBody"

const Graph = () => {
    return (
        <div className="col-lg-7 col-xl-8">
            <div className="card shadow mb-4">
                <ChartHeader heading="Budget tracker" Action1="last month" Action2="last hour" Other="last 10 years"></ChartHeader>
                <GraphBody></GraphBody>
            </div>
        </div>
    )
}

export default Graph;
