import SearchBar from "../Components/SearchBar";
import NotificationCenter from "../Components/NotificationCenter";
import Inbox from "../Components/Inbox";
import Profile from "../Components/Profile"


const NavBar = () => {
    return (
        <nav className="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
            <div className="container-fluid">
                <SearchBar></SearchBar>
                <ul className="navbar-nav flex-nowrap ml-auto">
                    <NotificationCenter></NotificationCenter>
                    <Inbox></Inbox>
                    <Profile></Profile>
                </ul>

            </div>
        </nav>
    )
}
export default NavBar;