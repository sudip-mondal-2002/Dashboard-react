const Calendar = () => {
    return (
            <div className="col-lg-6 col-md-12 col-sm-12">
                <div className="bootstrap_calendar">
                    <div className="container py-5">
                        <div className="calendar shadow bg-white p-5">
                            <div className="d-flex align-items-center"><i
                                className="fa fa-calendar fa-3x mr-3"></i>
                                <h2 className="month font-weight-bold mb-0 text-uppercase">April 2021
                                </h2>
                            </div>
                            <p className="font-italic text-muted mb-5">No events for this day.</p>
                            <ol className="day-names list-unstyled">
                                <li className="font-weight-bold text-uppercase">S</li>
                                <li className="font-weight-bold text-uppercase">M</li>
                                <li className="font-weight-bold text-uppercase">T</li>
                                <li className="font-weight-bold text-uppercase">W</li>
                                <li className="font-weight-bold text-uppercase">T</li>
                                <li className="font-weight-bold text-uppercase">F</li>
                                <li className="font-weight-bold text-uppercase">S</li>
                            </ol>

                            <ol className="days list-unstyled">
                            <li className="outside">
                                    <div className="date">28</div>
                                </li>
                                <li className="outside">
                                    <div className="date">29</div>
                                </li>
                                <li className="outside">
                                    <div className="date">30</div>
                                </li>
                                <li className="outside">
                                    <div className="date">31</div>
                                </li>
                                <li>
                                    <div className="date event bg-primary">1</div>
                                </li>
                                <li>
                                    <div className="date">2</div>
                                </li>
                                <li>
                                    <div className="date">3</div>
                                </li>
                                <li>
                                    <div className="date event bg-primary">4</div>
                                </li>
                                <li>
                                    <div className="date">5</div>
                                </li>
                                <li>
                                    <div className="date">6</div>
                                </li>
                                <li>
                                    <div className="date">7</div>
                                </li>
                                <li>
                                    <div className="date">8</div>
                                </li>
                                <li>
                                    <div className="date">9</div>
                                </li>
                                <li>
                                    <div className="date">10</div>
                                </li>
                                <li>
                                    <div className="date">11</div>
                                </li>
                                <li>
                                    <div className="date">12</div>
                                </li>
                                <li>
                                    <div className="date">13</div>
                                </li>
                                <li>
                                    <div className="date event bg-primary">14</div>
                                </li>
                                <li>
                                    <div className="date">15</div>
                                </li>
                                <li>
                                    <div className="date">16</div>
                                </li>
                                <li>
                                    <div className="date">17</div>
                                </li>
                                <li>
                                    <div className="date">18</div>
                                </li>
                                <li>
                                    <div className="date">19</div>
                                </li>
                                <li>
                                    <div className="date">20</div>
                                </li>
                                <li>
                                    <div className="date">21</div>
                                </li>
                                <li>
                                    <div className="date">22</div>
                                </li>
                                <li>
                                    <div className="date">23</div>
                                </li>
                                <li>
                                    <div className="date">24</div>
                                </li>
                                <li>
                                    <div className="date">25</div>
                                </li>
                                <li>
                                    <div className="date">26</div>
                                </li>
                                <li>
                                    <div className="date">27</div>
                                </li>
                                <li>
                                    <div className="date">28</div>
                                </li>
                                <li>
                                    <div className="date">29</div>
                                </li>
                                <li>
                                    <div className="date">30</div>
                                </li>
                                <li className="outside">
                                    <div className="date">1</div>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Calendar;