import ToDoItem from "../Components/ToDoItem"
const ToDoList = () => {
    return(
    <div className="card">
        <div className="card-header py-3">
            <h6 className="text-primary font-weight-bold m-0">Todo List</h6>
        </div>
        <ul className="list-group list-group-flush">
            <ToDoItem heading="Fix bugs in website home page" time="10:30 AM" index="0"></ToDoItem>
            <ToDoItem heading="Apply For E-Cell manager post" time="11:30 AM" index="1"></ToDoItem>
        </ul>
    </div>)
}

export default ToDoList;