import StatCard from "../Components/StatCard";

const Stat = () => {
    return (
        <div className="row">
            <StatCard heading="Served Clients(last year)" text="1080" color="primary" icon="calendar"></StatCard>
            <StatCard heading="Earnings (monthly)" text="$250,000" color="success" icon="dollar-sign"></StatCard>
            <StatCard heading="Tasks" text="40%" color="info" icon="clipboard-list"></StatCard>
            <StatCard heading="Pending Requests" text="12" color="warning" icon="comments"></StatCard>
        </div>
    )
}

export default Stat;