import ChartHeader from "../Components/ChartHeader";
import PiChartBody from "../Components/PiChartBody"
const PiChart = () => {
    return (
        <div className="col-lg-5 col-xl-4">
            <div className="card shadow mb-4">
                <ChartHeader heading="Revenue Sources" Action1="Raw Dataset" Action2="Survey Details" Other="Bar Graph View"></ChartHeader>
                <PiChartBody></PiChartBody>
            </div>
        </div>
    )
}

export default PiChart;